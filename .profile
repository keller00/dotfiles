# Adding ~.bin to regular path folders
export PATH=$PATH:$HOME/sys161/bin:/opt/local/bin:$HOME/sys161/tools/bin:$HOME/.bin

# MacPorts Installer addition on 2017-05-08_at_21:56:54: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

# Add Scala to Path
export SCALA_HOME=/usr/local/share/scala
export PATH=$PATH:$SCALA_HOME/bin
# Make Scala not crash
export TERM=xterm-color

